"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanySchema = void 0;
const mongoose_1 = require("mongoose");
const uuid_1 = require("uuid");
const id = (0, uuid_1.v4)();
exports.CompanySchema = new mongoose_1.Schema({
    UUID: {
        type: String,
        required: true,
        unique: true,
        default: (0, uuid_1.v4)()
    },
    company_name: {
        type: String,
        required: true,
        default: null,
    },
    company_ceo: {
        type: String,
        required: true,
        default: null,
    },
    company_address: {
        type: String,
        required: true,
        default: null,
    },
    inception_date: {
        type: Number,
        required: true,
    },
    created_at: {
        type: Date,
        default: Date.now(),
    },
    updated_at: {
        type: Date,
        default: Date.now(),
    },
});
//# sourceMappingURL=copmany.schema.js.map