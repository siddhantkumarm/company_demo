
## Description
Company demo Project for only demo purpose.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# Create env
 - Create .env file on root directory
 - Copy .sample-env 
 - Change MONGO_URL

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod

# Open link and get a swagger view 
Swagger Link : [http://localhost:3000/api/v1/playground/](http://localhost:3000/api/v1/playground/)
```


## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - Siddhant


## License

Nest is [MIT licensed](LICENSE).
