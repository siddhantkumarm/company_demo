import { CompanyDTO } from './copmany.dto';
import { CompanyService } from './services/company.service';
export declare class CompanyController {
    private readonly companyService;
    constructor(companyService: CompanyService);
    create(res: any, createCompanyDto: CompanyDTO): Promise<void>;
    findAll(req: any, res: any): Promise<void>;
    findOne(req: any, res: any, UUID: string): Promise<void>;
}
