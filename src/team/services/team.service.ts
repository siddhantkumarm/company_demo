import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { Model, Types } from "mongoose";
import { CompanyDoc } from 'src/company/interfaces/company-dcoument.interface';
import { TeamDoc } from '../interfaces/team-document.interface';
import { CreateTeamDto, UpdateTeamDto } from '../team.dto';
import * as _ from "lodash";
import * as moment from "moment";


@Injectable()
export class TeamService {

  constructor(
    @InjectModel("Team")
    private readonly teamModal: Model<TeamDoc>,
    @InjectModel("Company")
    private readonly companyModal: Model<CompanyDoc>,
  ) {

  }
  async create(createTeamDto: CreateTeamDto) {
    const getCompany = await this.companyModal.findOne({UUID: createTeamDto.company});
    if(!getCompany){
      throw new Error("Invalid Company id")
    }else {
      const team = await this.teamModal.create(createTeamDto);
      return team;
    }
  }

  async findAll() {
    const getAllTeam = await this.teamModal.aggregate([
      {
        $lookup: {
          from: 'companies',
          localField: 'company',
          foreignField: 'UUID',
          as: "company"
        }
      },
    ])
    const mapTeam = _.map(getAllTeam, (v) => {
      return {
        UUID: v.UUID,
        team_lead_name: v.team_lead_name,
        company: _.map(v.company, (item) => {
          return {
            UUID: item.UUID,
            company_name: item.company_name,
            company_ceo: item.company_ceo,
            company_address: item.company_address,
            inception_date: moment(item.inception_date).format("DD-MM-YYYY")
          }
        })
      }
    })
    return mapTeam;
  }

 
}
