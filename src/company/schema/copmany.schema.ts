import { Schema } from "mongoose";
import * as mongoose from 'mongoose'
import { v4 as uuid } from 'uuid';
const id: string = uuid();


export const CompanySchema = new Schema({
    UUID: {
        type: String,
        required: true,
        unique: true   ,
        default:  uuid()
    },
    company_name: {
        type: String,
        required: true,
        default: null,
    },
    company_ceo: {
        type: String,
        required: true,
        default: null,
    },
    company_address: {
        type: String,
        required: true,
        default: null,
    },
    inception_date: {
        type: Number,
        required: true,
    },
    created_at: {
        type: Date,
        default: Date.now(),
      },
    updated_at: {
        type: Date,
        default: Date.now(),
    },


});
