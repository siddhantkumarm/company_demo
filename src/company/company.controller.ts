import { Controller, Get, Post, Body, Patch, Param, Delete, Res, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiHeader, ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { CompanyDTO, UpdateCompanyDto } from './copmany.dto';
import * as responseHandler from "../responseHandler";
import { CompanyService } from './services/company.service';
import { CompanyValidation } from './company.validation';
import { get as _get } from "lodash";
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@ApiBearerAuth()
@ApiTags("Company Module")
@Controller("api/v1/company")
export class CompanyController {
  constructor(private readonly companyService: CompanyService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  @ApiOperation({ summary: "Create company" })
  @ApiBody({
    type: CompanyDTO,
    required: true,
    description: "Company Payload",
  })
  async create(@Res() res: any,@Body() createCompanyDto: CompanyDTO) {
    try {
      CompanyValidation.createValidation(createCompanyDto);
      const company = await this.companyService.create(createCompanyDto);

      if(!company){
        responseHandler.errorResponse(res, 500, "Internal Server error", []);

      }else {
        responseHandler.successResponse(
          res,
          201,
          "Company created successfully",
          company
        );
      }
      
    } catch (error) {
      responseHandler.errorResponse(res, 400, error.message, []);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  @ApiOperation({ summary: "Get All Company" }) 
  @ApiQuery({ name: "page", example: 1, required: false })
  @ApiQuery({ name: "limit", example: 6, required: false })
  @ApiQuery({ name: "search", example: "tcs", required: false })
  async findAll(@Req() req: any, @Res() res: any) {
    try {
      const page = Number(_get(req, "query.page", 1));
      const limit = Number(_get(req, "query.limit", 10));
      const search = _get(req, "query.search", null);

      const allCompany = await this.companyService.findAll({page, limit, search});

      if(!allCompany){
        responseHandler.errorResponse(res, 500, "Internal Server error", []);

      }else {
        responseHandler.successResponse(
          res,
          200,
          "Get all company's",
          allCompany
        );
      }
      
    } catch (error) {
      responseHandler.errorResponse(res, 400, error.message, []);
    }
    
  }

  @UseGuards(JwtAuthGuard)
  @Get(':UUID')
  @ApiOperation({ summary: "Get Company by UUID" })
  async findOne(@Req() req: any, @Res() res: any, @Param('UUID') UUID: string) {
    try {
     
      const allCompany = await this.companyService.findOne(UUID);
      if(!allCompany){
        responseHandler.errorResponse(res, 500, "Data Not found", []);

      }else {
        responseHandler.successResponse(
          res,
          200,
          "Data Found successfully",
          allCompany
        );
      }
      
    } catch (error) {
      responseHandler.errorResponse(res, 400, error.message, []);
    }
    
  }
}
