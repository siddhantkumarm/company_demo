export const successResponse = (res: any, status: number, message: string, data?: any, error?: any) => {
    res.status(status).send({
        code: "OK",
        message,
        statusCode: status,
        data
    });
}

export const errorResponse = (res: any, status: number, message: string, data?: any, error?: any) => {
    res.status(status).send({
        code: "E_ERROR_OCCURRED",
        message,
        statusCode: status,
        data
    });
}


