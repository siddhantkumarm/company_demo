import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from "mongoose";
import { CompanyDTO, UpdateCompanyDto } from '../copmany.dto';
import { CompanyDoc } from '../interfaces/company-dcoument.interface';
import * as _ from "lodash";
import * as moment from "moment";

@Injectable()
export class CompanyService {

  constructor(@InjectModel("Company")
  private readonly companyModal: Model<CompanyDoc>,) {

  }

  async create(createCompanyDto: CompanyDTO) {
    const company = await this.companyModal.create(createCompanyDto);
    return company;
  }

  async findAll(requestData: any) {
    let queryData: any = {};

    const skip =
      Number(requestData.page) === 1
        ? 0
        : Number(requestData.limit) * (Number(requestData.page) - 1);
    const limit = Number(requestData.limit);
    if (requestData.search != null) {
      queryData.company_name = { '$regex': requestData.search }
    }
    console.log(requestData)
    const company = await this.companyModal.find(queryData).limit(limit).skip(skip);

    return _.map(company, (item) => {
      return {
        UUID: item.UUID,
        company_name: item.company_name,
        company_ceo: item.company_ceo,
        company_address: item.company_address,
        inception_date: moment(item.inception_date).format("DD-MM-YYYY")
      }
    });
  }

  async findOne(UUID: string) {
    const findData: any = await this.companyModal.findOne({ UUID: UUID })
    const { company_name, company_ceo, company_address, inception_date } = findData
    return {
      UUID,
      company_name,
      company_ceo,
      company_address,
      inception_date: moment(inception_date).format("DD-MM-YYY")
    }
  }
}
