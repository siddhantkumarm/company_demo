import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
    constructor(
        private jwtService: JwtService
    ) { }

    async validateUser(username: string, pass: string): Promise<any> {
        const user = {
            "username": "siddhant",
            "DBId": "100",
            "firstName": "Siddhant",
            "lastName": "Kumar",
            "password": "kite12345"

        };
        if (user.username === username && user.password === pass) {
            const { DBId, ...result } = user;
            console.log(result);
            return result;
        }
        return null;
    }

    async login(user: any) {
        const payload = { DBId: user.DBId, firstName: user.firstName, lastName: user.lastName};
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
}
