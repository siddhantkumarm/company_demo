
import { ApiProperty } from "@nestjs/swagger";
import {
    IsNotEmpty,
    IsString,
} from "class-validator";

export class CreateTeamDto {
    @ApiProperty({
        description: "Company UUID",
        example: "403d5d23-a424-4c83-bcfd-aa5e4d85f513",
    })
    @IsNotEmpty()
    @IsString()
    company: string;

    @ApiProperty({
        description: "Team Lead Nam",
        example: "Test Name",
    })
    @IsNotEmpty()
    @IsString()
    team_lead_name: string;
}




export class UpdateTeamDto { }