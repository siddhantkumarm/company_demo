import { Schema } from "mongoose";
import * as mongoose from 'mongoose'
import { v4 as uuid } from 'uuid';


export const TeamSchema = new Schema({
    UUID: {
        type: String,
        required: true,
        unique: true   ,
        default:  uuid()
    },
    company: {
        type: String,
        required: true,
        default: null,
    },
    team_lead_name: {
        type: String,
        required: true,
        default: null,
    },
    created_at: {
        type: Date,
        default: Date.now(),
      },
    updated_at: {
        type: Date,
        default: Date.now(),
    },


});
