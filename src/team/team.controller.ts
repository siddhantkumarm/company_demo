import { Controller, Get, Post, Body, Patch, Param, Delete, Res, UseGuards } from '@nestjs/common';
import { ApiBody, ApiOperation, ApiTags } from '@nestjs/swagger';
import { TeamService } from './services/team.service';
import { CreateTeamDto, UpdateTeamDto } from './team.dto';
import { TeamValidation } from './team.validation';
import * as responseHandler from "../responseHandler";
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';


@ApiTags("Team Module")
@Controller("api/v1/team")
export class TeamController {
  constructor(private readonly teamService: TeamService) {}
  
  @UseGuards(JwtAuthGuard)
  @Post()
  @ApiOperation({ summary: "Create team" })
  @ApiBody({
    type: CreateTeamDto,
    required: true,
    description: "Team Payload",
  })
  async create(@Res() res: any, @Body() createTeamDto: CreateTeamDto) {
    try {
      TeamValidation.createValidation(createTeamDto);
      const team = await this.teamService.create(createTeamDto);

      if(!team){
        responseHandler.errorResponse(res, 500, "Internal Server error", []);

      }else {
        responseHandler.successResponse(
          res,
          201,
          "Team created successfully",
          team
        );
      }
      
    } catch (error) {
      responseHandler.errorResponse(res, 400, error.message, []);
    } 
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  @ApiOperation({ summary: "Get all team" })
  async findAll(@Res() res: any) {
    try {
      const getAllTeam = await this.teamService.findAll();
      if(!getAllTeam){
        responseHandler.errorResponse(res, 500, "Internal Server error", []);

      }else {
        responseHandler.successResponse(
          res,
          200,
          "Data Found successfully",
          getAllTeam
        );
      }
      
    } catch (error) {
      responseHandler.errorResponse(res, 400, error.message, []);
    }
  }

 
}
