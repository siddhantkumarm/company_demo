import { CompanyController } from './company.controller';
import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { HttpModule } from '@nestjs/axios';
import { MongooseModule } from '@nestjs/mongoose';
import { CompanySchema } from './schema/copmany.schema';
import { CompanyService } from './services/company.service';
@Module({
  imports:[
    HttpModule,
    ConfigModule.forRoot(),
    MongooseModule.forFeature([
      { name: "Company", schema: CompanySchema },
    ]),
  ],
  controllers: [CompanyController],
  providers: [CompanyService]
})
export class CompanyModule {}
