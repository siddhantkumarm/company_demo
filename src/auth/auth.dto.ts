import { ApiProperty } from "@nestjs/swagger";
import {
    IsNotEmpty,
    IsString,
} from "class-validator";

export class AuthLoginDto {
    @ApiProperty({
        description: "User name",
        example: "siddhant",
    })
    @IsNotEmpty()
    @IsString()
    username: string;

    @ApiProperty({
        description: "Password",
        example: "kite12345",
    })
    @IsNotEmpty()
    @IsString()
    password: string;
}

