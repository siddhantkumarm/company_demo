"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorResponse = exports.successResponse = void 0;
const successResponse = (res, status, message, data, error) => {
    res.status(status).send({
        code: "OK",
        message,
        statusCode: status,
        data
    });
};
exports.successResponse = successResponse;
const errorResponse = (res, status, message, data, error) => {
    res.status(status).send({
        code: "E_ERROR_OCCURRED",
        message,
        statusCode: status,
        data
    });
};
exports.errorResponse = errorResponse;
//# sourceMappingURL=responseHandler.js.map