import { Model } from "mongoose";
import { CompanyDTO } from '../copmany.dto';
import { CompanyDoc } from '../interfaces/company-dcoument.interface';
export declare class CompanyService {
    private readonly companyModal;
    constructor(companyModal: Model<CompanyDoc>);
    create(createCompanyDto: CompanyDTO): Promise<CompanyDoc & {
        _id: any;
    }>;
    findAll(requestData: any): Promise<any>;
    findOne(UUID: string): Promise<{
        UUID: string;
        company_name: any;
        company_ceo: any;
        company_address: any;
        inception_date: string;
    }>;
}
