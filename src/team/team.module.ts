import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { CompanySchema } from 'src/company/schema/copmany.schema';
import { TeamSchema } from './schema/team.schema';
import { TeamService } from './services/team.service';
import { TeamController } from './team.controller';

@Module({
  imports:[
    HttpModule,
    ConfigModule.forRoot(),
    MongooseModule.forFeature([
      { name: "Team", schema: TeamSchema },
      { name: "Company", schema: CompanySchema },
    ]),
  ],
  controllers: [TeamController],
  providers: [TeamService]
})
export class TeamModule {}
