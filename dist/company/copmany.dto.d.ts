export declare class CompanyDTO {
    company_name: string;
    company_ceo: string;
    company_address: string;
    inception_date: number;
}
export declare class UpdateCompanyDto {
}
