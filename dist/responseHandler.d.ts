export declare const successResponse: (res: any, status: number, message: string, data?: any, error?: any) => void;
export declare const errorResponse: (res: any, status: number, message: string, data?: any, error?: any) => void;
