"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const _ = require("lodash");
const moment = require("moment");
let CompanyService = class CompanyService {
    constructor(companyModal) {
        this.companyModal = companyModal;
    }
    async create(createCompanyDto) {
        const company = await this.companyModal.create(createCompanyDto);
        return company;
    }
    async findAll(requestData) {
        let queryData = {};
        const skip = Number(requestData.page) === 1
            ? 0
            : Number(requestData.limit) * (Number(requestData.page) - 1);
        const limit = Number(requestData.limit);
        if (requestData.search != null) {
            queryData.company_name = { '$regex': requestData.search };
        }
        console.log(requestData);
        const company = await this.companyModal.find(queryData).limit(limit).skip(skip);
        return _.map(company, (item) => {
            return {
                UUID: item.UUID,
                company_name: item.company_name,
                company_ceo: item.company_ceo,
                company_address: item.company_address,
                inception_date: moment(item.inception_date).format("DD-MM-YYYY")
            };
        });
    }
    async findOne(UUID) {
        const findData = await this.companyModal.findOne({ UUID: UUID });
        const { company_name, company_ceo, company_address, inception_date } = findData;
        return {
            UUID,
            company_name,
            company_ceo,
            company_address,
            inception_date: moment(inception_date).format("DD-MM-YYY")
        };
    }
};
CompanyService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)("Company")),
    __metadata("design:paramtypes", [mongoose_2.Model])
], CompanyService);
exports.CompanyService = CompanyService;
//# sourceMappingURL=company.service.js.map