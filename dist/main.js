"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const swagger_1 = require("@nestjs/swagger");
const mongoose = require("mongoose");
const express = require("express");
async function bootstrap() {
    var _a;
    const app = await core_1.NestFactory.create(app_module_1.AppModule, {});
    app.use(express.json({ limit: "16mb" }));
    app.use(express.urlencoded({ limit: "16mb", extended: true }));
    mongoose.set('debug', true);
    app.enableCors({
        origin: ((_a = process.env.ALLOWED_ORIGINS) === null || _a === void 0 ? void 0 : _a.split(",")) || true,
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
        credentials: true,
        maxAge: 60 * 60 * 24 * 31,
    });
    app.set("trust proxy", true);
    const options = new swagger_1.DocumentBuilder()
        .setTitle("Company Demo API ")
        .setDescription("Company Demo API Playground")
        .setVersion("1.0")
        .addBearerAuth()
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options);
    swagger_1.SwaggerModule.setup("api/v1/playground", app, document);
    await app.listen(3000);
}
bootstrap();
//# sourceMappingURL=main.js.map