"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const copmany_dto_1 = require("./copmany.dto");
const responseHandler = require("../responseHandler");
const company_service_1 = require("./services/company.service");
const company_validation_1 = require("./company.validation");
const lodash_1 = require("lodash");
const jwt_auth_guard_1 = require("../auth/jwt-auth.guard");
let CompanyController = class CompanyController {
    constructor(companyService) {
        this.companyService = companyService;
    }
    async create(res, createCompanyDto) {
        try {
            company_validation_1.CompanyValidation.createValidation(createCompanyDto);
            const company = await this.companyService.create(createCompanyDto);
            if (!company) {
                responseHandler.errorResponse(res, 500, "Internal Server error", []);
            }
            else {
                responseHandler.successResponse(res, 201, "Company created successfully", company);
            }
        }
        catch (error) {
            responseHandler.errorResponse(res, 400, error.message, []);
        }
    }
    async findAll(req, res) {
        try {
            const page = Number((0, lodash_1.get)(req, "query.page", 1));
            const limit = Number((0, lodash_1.get)(req, "query.limit", 10));
            const search = (0, lodash_1.get)(req, "query.search", null);
            const allCompany = await this.companyService.findAll({ page, limit, search });
            if (!allCompany) {
                responseHandler.errorResponse(res, 500, "Internal Server error", []);
            }
            else {
                responseHandler.successResponse(res, 200, "Get all company's", allCompany);
            }
        }
        catch (error) {
            responseHandler.errorResponse(res, 400, error.message, []);
        }
    }
    async findOne(req, res, UUID) {
        try {
            const allCompany = await this.companyService.findOne(UUID);
            if (!allCompany) {
                responseHandler.errorResponse(res, 500, "Data Not found", []);
            }
            else {
                responseHandler.successResponse(res, 200, "Data Found successfully", allCompany);
            }
        }
        catch (error) {
            responseHandler.errorResponse(res, 400, error.message, []);
        }
    }
};
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: "Create company" }),
    (0, swagger_1.ApiBody)({
        type: copmany_dto_1.CompanyDTO,
        required: true,
        description: "Company Payload",
    }),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, copmany_dto_1.CompanyDTO]),
    __metadata("design:returntype", Promise)
], CompanyController.prototype, "create", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({ summary: "Get All Company" }),
    (0, swagger_1.ApiQuery)({ name: "page", example: 1, required: false }),
    (0, swagger_1.ApiQuery)({ name: "limit", example: 6, required: false }),
    (0, swagger_1.ApiQuery)({ name: "search", example: "tcs", required: false }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CompanyController.prototype, "findAll", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Get)(':UUID'),
    (0, swagger_1.ApiOperation)({ summary: "Get Company by UUID" }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Res)()),
    __param(2, (0, common_1.Param)('UUID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, String]),
    __metadata("design:returntype", Promise)
], CompanyController.prototype, "findOne", null);
CompanyController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)("Company Module"),
    (0, common_1.Controller)("api/v1/company"),
    __metadata("design:paramtypes", [company_service_1.CompanyService])
], CompanyController);
exports.CompanyController = CompanyController;
//# sourceMappingURL=company.controller.js.map