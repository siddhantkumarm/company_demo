import { CompanyDTO } from "./copmany.dto";
import * as moment from "moment";

export class CompanyValidation {
    public static createValidation(company: CompanyDTO) {
        const gDate = moment(company.inception_date);
        if (company.company_name === undefined || company.company_name === "") {
            throw new Error("Company is required!");
        } else if (company.company_ceo === undefined || company.company_ceo === "") {
            throw new Error("Company CEO is required!");
        } else if (
            company.company_address === undefined || company.company_address === ""
        ) {
            throw new Error("Company Address is required!");
        }else if(company.inception_date === undefined || company.inception_date === 0){
            throw new Error("Company inception is required!");
        }else if(gDate.isValid()){
            throw new Error("Invalid Inception date!");
        }
    }
}
