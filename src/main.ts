import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { NestExpressApplication } from "@nestjs/platform-express";
import * as mongoose from 'mongoose';

import * as express from "express";


async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {});
  app.use(express.json({ limit: "16mb" }));
  app.use(express.urlencoded({ limit: "16mb", extended: true }));
  mongoose.set('debug', true);
  app.enableCors({
    origin: process.env.ALLOWED_ORIGINS?.split(",") || true,
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
    credentials: true,
    /**
     * Allowed 31 days for all cors request
     * Allowed all headers
     */
    maxAge: 60 * 60 * 24 * 31,
  });

  app.set("trust proxy", true);
  const options = new DocumentBuilder()
    .setTitle("Company Demo API ")
    .setDescription("Company Demo API Playground")
    .setVersion("1.0")
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup("api/v1/playground", app, document);
  await app.listen(3000);
}
bootstrap();
