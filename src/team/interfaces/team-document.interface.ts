import { Document } from "mongoose";


export interface TeamDoc extends Document {
    UUID?: string,
    company: string,
    team_lead_name: string,
    created_at?: Date,
    updated_at?: Date,
}