import { CreateTeamDto } from "./team.dto";

export class TeamValidation {
    public static createValidation(team: CreateTeamDto) {
        if (team.company === undefined || team.company === "") {
            throw new Error("Company Id is required!");
        } else if (team.team_lead_name === undefined || team.team_lead_name === "") {
            throw new Error("Team Lead name is required!");
        }
    }
}
