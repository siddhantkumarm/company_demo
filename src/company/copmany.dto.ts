import { ApiProperty } from "@nestjs/swagger";
import {
    IsNotEmpty,
  IsNumber,
    IsString,
  } from "class-validator";
export class CompanyDTO {
    @ApiProperty({
        description: "Company Name",
        example: "tcs",
      })
      @IsNotEmpty()
      @IsString()
      company_name: string;
    
      @ApiProperty({
        description: "Company ceo",
        example: "Test Name",
      })
      @IsNotEmpty()
      @IsString()
      company_ceo: string;

      @ApiProperty({
        description: "Company Address",
        example: "c-3328,ground floor, delhi",
      })
      @IsNotEmpty()
      @IsString()
      company_address: string;
  

      @ApiProperty({
        description: "Inception Date In millisecond",
        example: 213123123123,
      })
      @IsNotEmpty()
      @IsNumber()
      inception_date: number;
}

export class UpdateCompanyDto {
  
}
