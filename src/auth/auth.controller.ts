import { Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { AuthLoginDto } from './auth.dto';
import { LocalAuthGuard } from './local-auth.guard';

@ApiTags("Auth")
@Controller('api/v1')
export class AuthController {
    constructor(private authService: AuthService) { }

    @UseGuards(LocalAuthGuard)
    @Post('/auth/login')
    @ApiBody({
      type: AuthLoginDto,
      required: true,
      description: "Login Payload",
     })
    async login(@Request() req) {
        return this.authService.login(req.user);
    }

    // @Get('/profile')
    // @ApiBearerAuth()
    // getProfile(@Request() req) {
    //     return req.user;
    // }

}
