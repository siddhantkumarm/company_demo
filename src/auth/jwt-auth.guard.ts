import { Injectable, UnauthorizedException } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";

@Injectable()
export class JwtAuthGuard extends AuthGuard("jwt") {
  // handleRequest(err, user) {
  //   if (err || !user) {
  //     const error: any = {};
  //     error.message = "Token expired";
  //     throw err || new UnauthorizedException(error);
  //   }
  //   return user;
  // }
}
