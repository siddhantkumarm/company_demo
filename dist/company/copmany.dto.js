"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateCompanyDto = exports.CompanyDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class CompanyDTO {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        description: "Company Name",
        example: "tcs",
    }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CompanyDTO.prototype, "company_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: "Company ceo",
        example: "Test Name",
    }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CompanyDTO.prototype, "company_ceo", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: "Company Address",
        example: "c-3328,ground floor, delhi",
    }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CompanyDTO.prototype, "company_address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: "Inception Date In millisecond",
        example: 213123123123,
    }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], CompanyDTO.prototype, "inception_date", void 0);
exports.CompanyDTO = CompanyDTO;
class UpdateCompanyDto {
}
exports.UpdateCompanyDto = UpdateCompanyDto;
//# sourceMappingURL=copmany.dto.js.map