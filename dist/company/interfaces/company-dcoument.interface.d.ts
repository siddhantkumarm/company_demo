import { Document } from "mongoose";
export interface CompanyDoc extends Document {
    UUID?: string;
    company_name: string;
    company_ceo: string;
    company_address: string;
    inception_date: number;
    created_at?: Date;
    updated_at?: Date;
}
